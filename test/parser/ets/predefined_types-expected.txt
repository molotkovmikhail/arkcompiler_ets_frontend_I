{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "b",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 17,
                  "column": 5
                },
                "end": {
                  "line": 17,
                  "column": 6
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSPrimitiveType",
              "loc": {
                "start": {
                  "line": 17,
                  "column": 8
                },
                "end": {
                  "line": 17,
                  "column": 12
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "s",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 18,
                  "column": 5
                },
                "end": {
                  "line": 18,
                  "column": 6
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSPrimitiveType",
              "loc": {
                "start": {
                  "line": 18,
                  "column": 8
                },
                "end": {
                  "line": 18,
                  "column": 13
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "i",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 19,
                  "column": 5
                },
                "end": {
                  "line": 19,
                  "column": 6
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSPrimitiveType",
              "loc": {
                "start": {
                  "line": 19,
                  "column": 8
                },
                "end": {
                  "line": 19,
                  "column": 11
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "l",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 20,
                  "column": 5
                },
                "end": {
                  "line": 20,
                  "column": 6
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSPrimitiveType",
              "loc": {
                "start": {
                  "line": 20,
                  "column": 8
                },
                "end": {
                  "line": 20,
                  "column": 12
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "f",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 22,
                  "column": 5
                },
                "end": {
                  "line": 22,
                  "column": 6
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSPrimitiveType",
              "loc": {
                "start": {
                  "line": 22,
                  "column": 8
                },
                "end": {
                  "line": 22,
                  "column": 13
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "d",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 23,
                  "column": 5
                },
                "end": {
                  "line": 23,
                  "column": 6
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSPrimitiveType",
              "loc": {
                "start": {
                  "line": 23,
                  "column": 8
                },
                "end": {
                  "line": 23,
                  "column": 14
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "c",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 25,
                  "column": 5
                },
                "end": {
                  "line": 25,
                  "column": 6
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSPrimitiveType",
              "loc": {
                "start": {
                  "line": 25,
                  "column": 8
                },
                "end": {
                  "line": 25,
                  "column": 12
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 27,
      "column": 1
    }
  }
}
