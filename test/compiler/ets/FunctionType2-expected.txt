{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "func",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 5
                },
                "end": {
                  "line": 1,
                  "column": 9
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSFunctionType",
              "params": [
                {
                  "type": "Identifier",
                  "name": "a",
                  "typeAnnotation": {
                    "type": "ETSPrimitiveType",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 15
                      },
                      "end": {
                        "line": 1,
                        "column": 18
                      }
                    }
                  },
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 12
                    },
                    "end": {
                      "line": 1,
                      "column": 18
                    }
                  }
                },
                {
                  "type": "Identifier",
                  "name": "b",
                  "typeAnnotation": {
                    "type": "ETSPrimitiveType",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 23
                      },
                      "end": {
                        "line": 1,
                        "column": 26
                      }
                    }
                  },
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 20
                    },
                    "end": {
                      "line": 1,
                      "column": 26
                    }
                  }
                }
              ],
              "returnType": {
                "type": "ETSPrimitiveType",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 31
                  },
                  "end": {
                    "line": 1,
                    "column": 37
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 11
                },
                "end": {
                  "line": 1,
                  "column": 37
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "func2",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 3,
                  "column": 10
                },
                "end": {
                  "line": 3,
                  "column": 15
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "func2",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 10
                    },
                    "end": {
                      "line": 3,
                      "column": 15
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "Identifier",
                    "name": "a",
                    "typeAnnotation": {
                      "type": "ETSFunctionType",
                      "params": [
                        {
                          "type": "Identifier",
                          "name": "a",
                          "typeAnnotation": {
                            "type": "ETSPrimitiveType",
                            "loc": {
                              "start": {
                                "line": 3,
                                "column": 23
                              },
                              "end": {
                                "line": 3,
                                "column": 26
                              }
                            }
                          },
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 3,
                              "column": 20
                            },
                            "end": {
                              "line": 3,
                              "column": 26
                            }
                          }
                        },
                        {
                          "type": "Identifier",
                          "name": "b",
                          "typeAnnotation": {
                            "type": "ETSPrimitiveType",
                            "loc": {
                              "start": {
                                "line": 3,
                                "column": 31
                              },
                              "end": {
                                "line": 3,
                                "column": 34
                              }
                            }
                          },
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 3,
                              "column": 28
                            },
                            "end": {
                              "line": 3,
                              "column": 34
                            }
                          }
                        }
                      ],
                      "returnType": {
                        "type": "ETSPrimitiveType",
                        "loc": {
                          "start": {
                            "line": 3,
                            "column": 39
                          },
                          "end": {
                            "line": 3,
                            "column": 45
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 3,
                          "column": 19
                        },
                        "end": {
                          "line": 3,
                          "column": 45
                        }
                      }
                    },
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 3,
                        "column": 16
                      },
                      "end": {
                        "line": 3,
                        "column": 45
                      }
                    }
                  }
                ],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 48
                    },
                    "end": {
                      "line": 3,
                      "column": 52
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 53
                    },
                    "end": {
                      "line": 5,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 3,
                    "column": 15
                  },
                  "end": {
                    "line": 5,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 3,
                  "column": 15
                },
                "end": {
                  "line": 5,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 3,
                "column": 1
              },
              "end": {
                "line": 5,
                "column": 2
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 7,
                  "column": 10
                },
                "end": {
                  "line": 7,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 10
                    },
                    "end": {
                      "line": 7,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 18
                    },
                    "end": {
                      "line": 7,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "CallExpression",
                        "callee": {
                          "type": "Identifier",
                          "name": "func2",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 8,
                              "column": 5
                            },
                            "end": {
                              "line": 8,
                              "column": 10
                            }
                          }
                        },
                        "arguments": [
                          {
                            "type": "Identifier",
                            "name": "func",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 8,
                                "column": 11
                              },
                              "end": {
                                "line": 8,
                                "column": 15
                              }
                            }
                          }
                        ],
                        "optional": false,
                        "loc": {
                          "start": {
                            "line": 8,
                            "column": 5
                          },
                          "end": {
                            "line": 8,
                            "column": 16
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 8,
                          "column": 5
                        },
                        "end": {
                          "line": 8,
                          "column": 17
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 23
                    },
                    "end": {
                      "line": 9,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 7,
                    "column": 14
                  },
                  "end": {
                    "line": 9,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 7,
                  "column": 14
                },
                "end": {
                  "line": 9,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 7,
                "column": 1
              },
              "end": {
                "line": 9,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 10,
      "column": 1
    }
  }
}
