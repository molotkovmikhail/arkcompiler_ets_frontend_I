{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "bar",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 5
                },
                "end": {
                  "line": 2,
                  "column": 8
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "bar",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 5
                    },
                    "end": {
                      "line": 2,
                      "column": 8
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 12
                    },
                    "end": {
                      "line": 2,
                      "column": 16
                    }
                  }
                },
                "declare": true,
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 8
                  },
                  "end": {
                    "line": 2,
                    "column": 16
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 8
                },
                "end": {
                  "line": 2,
                  "column": 16
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 16
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 13
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "A",
        "decorators": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 11
          },
          "end": {
            "line": 1,
            "column": 12
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 9
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "B",
          "decorators": [],
          "loc": {
            "start": {
              "line": 5,
              "column": 16
            },
            "end": {
              "line": 5,
              "column": 17
            }
          }
        },
        "superClass": null,
        "implements": [
          {
            "type": "TSClassImplements",
            "expression": {
              "type": "ETSTypeReference",
              "part": {
                "type": "ETSTypeReferencePart",
                "name": {
                  "type": "Identifier",
                  "name": "A",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 29
                    },
                    "end": {
                      "line": 5,
                      "column": 30
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 29
                  },
                  "end": {
                    "line": 5,
                    "column": 32
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 5,
                  "column": 29
                },
                "end": {
                  "line": 5,
                  "column": 32
                }
              }
            },
            "loc": {
              "start": {
                "line": 5,
                "column": 29
              },
              "end": {
                "line": 5,
                "column": 32
              }
            }
          }
        ],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "bar",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 14
                },
                "end": {
                  "line": 6,
                  "column": 17
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "bar",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 6,
                      "column": 14
                    },
                    "end": {
                      "line": 6,
                      "column": 17
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 6,
                      "column": 21
                    },
                    "end": {
                      "line": 6,
                      "column": 25
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 6,
                      "column": 25
                    },
                    "end": {
                      "line": 8,
                      "column": 6
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 6,
                    "column": 17
                  },
                  "end": {
                    "line": 8,
                    "column": 6
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 6,
                  "column": 17
                },
                "end": {
                  "line": 8,
                  "column": 6
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 6,
                "column": 5
              },
              "end": {
                "line": 8,
                "column": 6
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 9,
                  "column": 14
                },
                "end": {
                  "line": 9,
                  "column": 17
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 9,
                      "column": 14
                    },
                    "end": {
                      "line": 9,
                      "column": 17
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 9,
                      "column": 21
                    },
                    "end": {
                      "line": 9,
                      "column": 25
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 9,
                    "column": 17
                  },
                  "end": {
                    "line": 9,
                    "column": 17
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 9,
                  "column": 17
                },
                "end": {
                  "line": 9,
                  "column": 17
                }
              }
            },
            "overloads": [
              {
                "type": "MethodDefinition",
                "key": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 10,
                      "column": 14
                    },
                    "end": {
                      "line": 10,
                      "column": 17
                    }
                  }
                },
                "kind": "method",
                "accessibility": "public",
                "static": false,
                "optional": false,
                "computed": false,
                "value": {
                  "type": "FunctionExpression",
                  "function": {
                    "type": "ScriptFunction",
                    "id": {
                      "type": "Identifier",
                      "name": "foo",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 10,
                          "column": 14
                        },
                        "end": {
                          "line": 10,
                          "column": 17
                        }
                      }
                    },
                    "generator": false,
                    "async": false,
                    "expression": false,
                    "params": [
                      {
                        "type": "Identifier",
                        "name": "a",
                        "typeAnnotation": {
                          "type": "ETSPrimitiveType",
                          "loc": {
                            "start": {
                              "line": 10,
                              "column": 21
                            },
                            "end": {
                              "line": 10,
                              "column": 24
                            }
                          }
                        },
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 10,
                            "column": 18
                          },
                          "end": {
                            "line": 10,
                            "column": 24
                          }
                        }
                      }
                    ],
                    "returnType": {
                      "type": "ETSPrimitiveType",
                      "loc": {
                        "start": {
                          "line": 10,
                          "column": 27
                        },
                        "end": {
                          "line": 10,
                          "column": 31
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 10,
                        "column": 17
                      },
                      "end": {
                        "line": 10,
                        "column": 17
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 10,
                      "column": 17
                    },
                    "end": {
                      "line": 10,
                      "column": 17
                    }
                  }
                },
                "overloads": [],
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 10,
                    "column": 5
                  },
                  "end": {
                    "line": 10,
                    "column": 17
                  }
                }
              },
              {
                "type": "MethodDefinition",
                "key": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 11,
                      "column": 5
                    },
                    "end": {
                      "line": 11,
                      "column": 8
                    }
                  }
                },
                "kind": "method",
                "accessibility": "public",
                "static": false,
                "optional": false,
                "computed": false,
                "value": {
                  "type": "FunctionExpression",
                  "function": {
                    "type": "ScriptFunction",
                    "id": {
                      "type": "Identifier",
                      "name": "foo",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 11,
                          "column": 5
                        },
                        "end": {
                          "line": 11,
                          "column": 8
                        }
                      }
                    },
                    "generator": false,
                    "async": false,
                    "expression": false,
                    "params": [
                      {
                        "type": "Identifier",
                        "name": "b",
                        "typeAnnotation": {
                          "type": "ETSPrimitiveType",
                          "loc": {
                            "start": {
                              "line": 11,
                              "column": 12
                            },
                            "end": {
                              "line": 11,
                              "column": 18
                            }
                          }
                        },
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 11,
                            "column": 9
                          },
                          "end": {
                            "line": 11,
                            "column": 18
                          }
                        }
                      }
                    ],
                    "returnType": {
                      "type": "ETSPrimitiveType",
                      "loc": {
                        "start": {
                          "line": 11,
                          "column": 21
                        },
                        "end": {
                          "line": 11,
                          "column": 25
                        }
                      }
                    },
                    "body": {
                      "type": "BlockStatement",
                      "statements": [],
                      "loc": {
                        "start": {
                          "line": 11,
                          "column": 25
                        },
                        "end": {
                          "line": 13,
                          "column": 6
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 11,
                        "column": 8
                      },
                      "end": {
                        "line": 13,
                        "column": 6
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 11,
                      "column": 8
                    },
                    "end": {
                      "line": 13,
                      "column": 6
                    }
                  }
                },
                "overloads": [],
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 11,
                    "column": 5
                  },
                  "end": {
                    "line": 13,
                    "column": 6
                  }
                }
              }
            ],
            "decorators": [],
            "loc": {
              "start": {
                "line": 9,
                "column": 5
              },
              "end": {
                "line": 9,
                "column": 17
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 14,
                "column": 2
              },
              "end": {
                "line": 14,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 5,
            "column": 31
          },
          "end": {
            "line": 14,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 5,
          "column": 10
        },
        "end": {
          "line": 14,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "C",
          "decorators": [],
          "loc": {
            "start": {
              "line": 16,
              "column": 7
            },
            "end": {
              "line": 16,
              "column": 8
            }
          }
        },
        "superClass": {
          "type": "ETSTypeReference",
          "part": {
            "type": "ETSTypeReferencePart",
            "name": {
              "type": "Identifier",
              "name": "B",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 16,
                  "column": 17
                },
                "end": {
                  "line": 16,
                  "column": 18
                }
              }
            },
            "loc": {
              "start": {
                "line": 16,
                "column": 17
              },
              "end": {
                "line": 16,
                "column": 20
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 17
            },
            "end": {
              "line": 16,
              "column": 20
            }
          }
        },
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 17,
                  "column": 14
                },
                "end": {
                  "line": 17,
                  "column": 17
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 14
                    },
                    "end": {
                      "line": 17,
                      "column": 17
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 21
                    },
                    "end": {
                      "line": 17,
                      "column": 25
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 26
                    },
                    "end": {
                      "line": 19,
                      "column": 6
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 17,
                    "column": 17
                  },
                  "end": {
                    "line": 19,
                    "column": 6
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 17,
                  "column": 17
                },
                "end": {
                  "line": 19,
                  "column": 6
                }
              }
            },
            "overloads": [
              {
                "type": "MethodDefinition",
                "key": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 14
                    },
                    "end": {
                      "line": 20,
                      "column": 17
                    }
                  }
                },
                "kind": "method",
                "accessibility": "public",
                "static": false,
                "optional": false,
                "computed": false,
                "value": {
                  "type": "FunctionExpression",
                  "function": {
                    "type": "ScriptFunction",
                    "id": {
                      "type": "Identifier",
                      "name": "foo",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 14
                        },
                        "end": {
                          "line": 20,
                          "column": 17
                        }
                      }
                    },
                    "generator": false,
                    "async": false,
                    "expression": false,
                    "params": [
                      {
                        "type": "Identifier",
                        "name": "a",
                        "typeAnnotation": {
                          "type": "ETSPrimitiveType",
                          "loc": {
                            "start": {
                              "line": 20,
                              "column": 21
                            },
                            "end": {
                              "line": 20,
                              "column": 24
                            }
                          }
                        },
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 20,
                            "column": 18
                          },
                          "end": {
                            "line": 20,
                            "column": 24
                          }
                        }
                      }
                    ],
                    "returnType": {
                      "type": "ETSPrimitiveType",
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 27
                        },
                        "end": {
                          "line": 20,
                          "column": 31
                        }
                      }
                    },
                    "body": {
                      "type": "BlockStatement",
                      "statements": [],
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 32
                        },
                        "end": {
                          "line": 22,
                          "column": 6
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 20,
                        "column": 17
                      },
                      "end": {
                        "line": 22,
                        "column": 6
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 17
                    },
                    "end": {
                      "line": 22,
                      "column": 6
                    }
                  }
                },
                "overloads": [],
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 20,
                    "column": 5
                  },
                  "end": {
                    "line": 22,
                    "column": 6
                  }
                }
              }
            ],
            "decorators": [],
            "loc": {
              "start": {
                "line": 17,
                "column": 5
              },
              "end": {
                "line": 19,
                "column": 6
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 23,
                "column": 2
              },
              "end": {
                "line": 23,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 16,
            "column": 19
          },
          "end": {
            "line": 23,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 23,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 24,
      "column": 1
    }
  }
}
