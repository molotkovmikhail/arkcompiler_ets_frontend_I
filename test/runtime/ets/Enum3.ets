/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

enum Color { Red, Green, Blue }

function main(): void throws {
  let x: int = Color.Red.ordinal();
  assert x == 0;

  let blue = Color.Blue;
  let str = blue.toString();
  assert "Blue" == str;

  let values = Color.values();
  assert values.length == 3;
  assert values[0] == Color.Red;
  assert values[1] == Color.Green;
  assert values[2] == Color.Blue;

  let red = Color.valueOf("Red");
  assert red == Color.Red;

  try {
    let yellow: Color = Color.valueOf("Yellow");
    assert false;
  } catch (e: Exception) {
    assert (e as Object).toString().startsWith("No enum constant Color.Yellow");
  }

  let one: int = 1;
  let green = one as Color;
  assert green == Color.Green;

  try {
    let x = 5 as Color;
    assert false;
  } catch (e: Exception) {
    assert (e as Object).toString().startsWith("No enum constant in Color with ordinal value 5");
  }

  assert 2 as Color as int == 2;
  assert Color.Blue as int as Color == Color.Blue;
  assert (Color.Red as int + 1) as Color == (Color.Blue as int - 1) as Color;
}
