/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "signature.h"

#include "plugins/ecmascript/es2panda/binder/scope.h"
#include "plugins/ecmascript/es2panda/ir/base/scriptFunction.h"
#include "plugins/ecmascript/es2panda/checker/ETSchecker.h"

namespace panda::es2panda::checker {

util::StringView Signature::InternalName() const
{
    return internal_name_.Empty() ? func_->Scope()->InternalName() : internal_name_;
}

Signature *Signature::Copy(ArenaAllocator *allocator, TypeRelation *relation, GlobalTypesHolder *global_types)
{
    SignatureInfo *copied_info = allocator->New<SignatureInfo>(signature_info_, allocator);

    for (size_t idx = 0; idx < signature_info_->params.size(); idx++) {
        copied_info->params[idx]->SetTsType(
            ETSChecker::TryToInstantiate(signature_info_->params[idx]->TsType(), allocator, relation, global_types));
    }

    Type *copied_return_type = ETSChecker::TryToInstantiate(return_type_, allocator, relation, global_types);

    return allocator->New<Signature>(copied_info, copied_return_type, func_);
}

void Signature::ToString(std::stringstream &ss, const binder::Variable *variable, bool print_as_method) const
{
    ss << "(";

    for (auto it = signature_info_->params.begin(); it != signature_info_->params.end(); it++) {
        ss << (*it)->Name();

        if ((*it)->HasFlag(binder::VariableFlags::OPTIONAL)) {
            ss << "?";
        }

        ss << ": ";

        (*it)->TsType()->ToString(ss);

        if (std::next(it) != signature_info_->params.end()) {
            ss << ", ";
        }
    }

    if (signature_info_->rest_var != nullptr) {
        if (!signature_info_->params.empty()) {
            ss << ", ";
        }

        ss << "...";
        ss << signature_info_->rest_var->Name();
        ss << ": ";
        signature_info_->rest_var->TsType()->ToString(ss);
        ss << "[]";
    }

    ss << ")";

    if (print_as_method || (variable != nullptr && variable->HasFlag(binder::VariableFlags::METHOD))) {
        ss << ": ";
    } else {
        ss << " => ";
    }

    return_type_->ToString(ss);
}

void Signature::Identical(TypeRelation *relation, Signature *other)
{
    if (signature_info_->min_arg_count != other->MinArgCount() ||
        signature_info_->params.size() != other->Params().size()) {
        relation->Result(false);
        return;
    }

    if (relation->NoReturnTypeCheck()) {
        relation->Result(true);
    } else {
        relation->IsIdenticalTo(return_type_, other->ReturnType());
    }

    if (relation->IsTrue()) {
        for (uint64_t i = 0; i < signature_info_->params.size(); i++) {
            if (!CheckFunctionalInterfaces(relation, signature_info_->params[i]->TsType(),
                                           other->Params()[i]->TsType())) {
                relation->IsIdenticalTo(signature_info_->params[i]->TsType(), other->Params()[i]->TsType());
            }

            if (!relation->IsTrue()) {
                return;
            }
        }

        if (signature_info_->rest_var != nullptr && other->RestVar() != nullptr) {
            relation->IsIdenticalTo(signature_info_->rest_var->TsType(), other->RestVar()->TsType());
        } else if ((signature_info_->rest_var != nullptr && other->RestVar() == nullptr) ||
                   (signature_info_->rest_var == nullptr && other->RestVar() != nullptr)) {
            relation->Result(false);
        }
    }
}

bool Signature::CheckFunctionalInterfaces(TypeRelation *relation, Type *source, Type *target)
{
    if (!source->IsETSObjectType() || !source->AsETSObjectType()->HasObjectFlag(ETSObjectFlags::FUNCTIONAL_INTERFACE)) {
        return false;
    }

    if (!target->IsETSObjectType() || !target->AsETSObjectType()->HasObjectFlag(ETSObjectFlags::FUNCTIONAL_INTERFACE)) {
        return false;
    }

    auto source_invoke_func = source->AsETSObjectType()
                                  ->GetProperty(util::StringView("invoke"), PropertySearchFlags::SEARCH_INSTANCE_METHOD)
                                  ->TsType()
                                  ->AsETSFunctionType()
                                  ->CallSignatures()[0];

    auto target_invoke_func = target->AsETSObjectType()
                                  ->GetProperty(util::StringView("invoke"), PropertySearchFlags::SEARCH_INSTANCE_METHOD)
                                  ->TsType()
                                  ->AsETSFunctionType()
                                  ->CallSignatures()[0];

    relation->IsIdenticalTo(source_invoke_func, target_invoke_func);
    return true;
}

void Signature::AssignmentTarget(TypeRelation *relation, Signature *source)
{
    if (signature_info_->rest_var == nullptr &&
        (source->Params().size() - source->OptionalArgCount()) > signature_info_->params.size()) {
        relation->Result(false);
        return;
    }

    for (size_t i = 0; i < source->Params().size(); i++) {
        if (signature_info_->rest_var == nullptr && i >= Params().size()) {
            break;
        }

        if (signature_info_->rest_var != nullptr) {
            relation->IsAssignableTo(source->Params()[i]->TsType(), signature_info_->rest_var->TsType());

            if (!relation->IsTrue()) {
                return;
            }

            continue;
        }

        relation->IsAssignableTo(source->Params()[i]->TsType(), Params()[i]->TsType());

        if (!relation->IsTrue()) {
            return;
        }
    }

    relation->IsAssignableTo(source->ReturnType(), return_type_);

    if (relation->IsTrue() && signature_info_->rest_var != nullptr && source->RestVar() != nullptr) {
        relation->IsAssignableTo(source->RestVar()->TsType(), signature_info_->rest_var->TsType());
    }
}
}  // namespace panda::es2panda::checker
