/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ES2PANDA_LEXER_TOKEN_NUMBER_H
#define ES2PANDA_LEXER_TOKEN_NUMBER_H

#include "macros.h"
#include "plugins/ecmascript/es2panda/util/ustring.h"
#include "plugins/ecmascript/es2panda/util/enumbitops.h"

#include <cstdint>

namespace panda::es2panda::lexer {
enum class NumberFlags : uint32_t {
    NONE,
    BIGINT = 1U << 0U,
    DECIMAL_POINT = 1U << 1U,
    EXPONENT = 1U << 2U,
    ERROR = 1U << 3U,
};

DEFINE_BITOPS(NumberFlags)

// NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init)
class Number {
public:
    // NOLINTBEGIN(cppcoreguidelines-pro-type-member-init)
    explicit Number() noexcept = default;
    explicit Number(util::StringView str) noexcept : str_(str) {}
    // NOLINTNEXTLINE(bugprone-exception-escape)
    explicit Number(util::StringView str, const std::string &utf8, NumberFlags flags) noexcept;
    explicit Number(util::StringView str, double num) noexcept : str_(str), num_(num) {}
    explicit Number(uint32_t num) noexcept : Number(static_cast<int32_t>(num)) {}
    explicit Number(int32_t num) noexcept : num_(num) {}
    explicit Number(uint64_t num) noexcept : Number(static_cast<int64_t>(num)) {}
    explicit Number(int64_t num) noexcept : num_(num) {}
    explicit Number(float num) noexcept : num_(num) {}
    explicit Number(double num) noexcept : num_(num) {}
    DEFAULT_COPY_SEMANTIC(Number);
    DEFAULT_MOVE_SEMANTIC(Number);
    ~Number() = default;
    // NOLINTEND(cppcoreguidelines-pro-type-member-init)

    bool IsInt() const
    {
        return std::holds_alternative<int32_t>(num_);
    }

    bool IsLong() const
    {
        return std::holds_alternative<int64_t>(num_);
    }

    bool IsFloat() const
    {
        return std::holds_alternative<float>(num_);
    }

    bool IsDouble() const
    {
        return std::holds_alternative<double>(num_);
    }

    bool ConversionError() const
    {
        return (flags_ & NumberFlags::ERROR) != 0;
    }

    int32_t GetInt() const
    {
        return std::get<int32_t>(num_);
    }

    int64_t GetLong() const
    {
        return std::get<int64_t>(num_);
    }

    float GetFloat() const
    {
        return std::get<float>(num_);
    }

    double GetDouble() const
    {
        if (IsInt()) {
            return static_cast<double>(GetInt());
        }
        if (IsLong()) {
            return static_cast<double>(GetLong());
        }
        if (IsFloat()) {
            return static_cast<double>(GetFloat());
        }
        ASSERT(IsDouble());
        return std::get<double>(num_);
    }

    const util::StringView &Str() const
    {
        return str_;
    }

private:
    util::StringView str_ {};
    std::variant<int32_t, int64_t, float, double> num_;
    NumberFlags flags_ {NumberFlags::NONE};
};
}  // namespace panda::es2panda::lexer

#endif
