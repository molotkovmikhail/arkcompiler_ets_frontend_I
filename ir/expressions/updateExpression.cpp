/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "updateExpression.h"

#include "plugins/ecmascript/es2panda/binder/variable.h"
#include "plugins/ecmascript/es2panda/compiler/base/lreference.h"
#include "plugins/ecmascript/es2panda/compiler/core/pandagen.h"
#include "plugins/ecmascript/es2panda/compiler/core/ETSGen.h"
#include "plugins/ecmascript/es2panda/compiler/core/regScope.h"
#include "plugins/ecmascript/es2panda/checker/TSchecker.h"
#include "plugins/ecmascript/es2panda/checker/ETSchecker.h"
#include "plugins/ecmascript/es2panda/ir/astDump.h"
#include "plugins/ecmascript/es2panda/ir/expressions/unaryExpression.h"
#include "plugins/ecmascript/es2panda/ir/expressions/identifier.h"
#include "plugins/ecmascript/es2panda/ir/expressions/memberExpression.h"

namespace panda::es2panda::ir {
void UpdateExpression::Iterate(const NodeTraverser &cb) const
{
    cb(argument_);
}

void UpdateExpression::Dump(ir::AstDumper *dumper) const
{
    dumper->Add({{"type", "UpdateExpression"}, {"operator", operator_}, {"prefix", prefix_}, {"argument", argument_}});
}

void UpdateExpression::Compile(compiler::PandaGen *pg) const
{
    compiler::RegScope rs(pg);
    compiler::VReg operand_reg = pg->AllocReg();

    auto lref = compiler::JSLReference::Create(pg, argument_, false);
    lref.GetValue();

    pg->StoreAccumulator(this, operand_reg);
    pg->Unary(this, operator_, operand_reg);

    lref.SetValue();

    if (!IsPrefix()) {
        pg->ToNumber(this, operand_reg);
    }
}

void UpdateExpression::Compile(compiler::ETSGen *etsg) const
{
    auto lref = compiler::ETSLReference::Create(etsg, argument_, false);
    lref.GetValue();

    if (prefix_) {
        etsg->ApplyConversion(this, argument_->TsType());
        etsg->Update(this, operator_);
        lref.SetValue();
        return;
    }

    compiler::RegScope rs(etsg);
    compiler::VReg operand_reg = etsg->AllocReg();
    etsg->ApplyConversionAndStoreAccumulator(this, operand_reg, argument_->TsType());
    etsg->Update(this, operator_);
    lref.SetValue();
    etsg->LoadAccumulator(this, operand_reg);
}

checker::Type *UpdateExpression::Check(checker::TSChecker *checker)
{
    checker::Type *operand_type = argument_->Check(checker);
    checker->CheckNonNullType(operand_type, Start());

    if (!operand_type->HasTypeFlag(checker::TypeFlag::VALID_ARITHMETIC_TYPE)) {
        checker->ThrowTypeError("An arithmetic operand must be of type 'any', 'number', 'bigint' or an enum type.",
                                Start());
    }

    checker->CheckReferenceExpression(
        argument_, "The operand of an increment or decrement operator must be a variable or a property access",
        "The operand of an increment or decrement operator may not be an optional property access");

    return checker->GetUnaryResultType(operand_type);
}

checker::Type *UpdateExpression::Check(checker::ETSChecker *checker)
{
    checker::Type *operand_type = argument_->Check(checker);
    if (argument_->IsIdentifier()) {
        checker->ValidateUnaryOperatorOperand(argument_->AsIdentifier()->Variable());
    } else {
        ASSERT(argument_->IsMemberExpression());
        binder::LocalVariable *prop_var = argument_->AsMemberExpression()->PropVar();
        if (prop_var != nullptr) {
            checker->ValidateUnaryOperatorOperand(prop_var);
        }
    }

    auto unboxed_type = checker->ETSBuiltinTypeAsPrimitiveType(operand_type);

    if (unboxed_type == nullptr || !unboxed_type->HasTypeFlag(checker::TypeFlag::ETS_NUMERIC)) {
        checker->ThrowTypeError("Bad operand type, the type of the operand must be numeric type.", argument_->Start());
    }

    if (operand_type->IsETSObjectType()) {
        argument_->AddBoxingUnboxingFlag(checker->GetUnboxingFlag(unboxed_type));
    }

    SetTsType(operand_type);
    return TsType();
}
}  // namespace panda::es2panda::ir
