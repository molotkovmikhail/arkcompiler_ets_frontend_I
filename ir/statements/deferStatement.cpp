/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "deferStatement.h"

#include "plugins/ecmascript/es2panda/compiler/core/pandagen.h"
#include "plugins/ecmascript/es2panda/compiler/core/ETSGen.h"
#include "plugins/ecmascript/es2panda/checker/ETSchecker.h"
#include "plugins/ecmascript/es2panda/checker/TSchecker.h"
#include "plugins/ecmascript/es2panda/ir/astDump.h"
#include "plugins/ecmascript/es2panda/ir/expression.h"

namespace panda::es2panda::ir {
void DeferStatement::Iterate(const NodeTraverser &cb) const
{
    cb(stmt_);
}

void DeferStatement::Dump(ir::AstDumper *dumper) const
{
    dumper->Add({{"type", "DeferStatement"}, {"stmt", stmt_}});
}

void DeferStatement::Compile([[maybe_unused]] compiler::PandaGen *pg) const {}

void DeferStatement::Compile([[maybe_unused]] compiler::ETSGen *etsg) const {}

checker::Type *DeferStatement::Check([[maybe_unused]] checker::TSChecker *checker)
{
    return nullptr;
}

checker::Type *DeferStatement::Check([[maybe_unused]] checker::ETSChecker *checker)
{
    return stmt_->Check(checker);
}
}  // namespace panda::es2panda::ir
