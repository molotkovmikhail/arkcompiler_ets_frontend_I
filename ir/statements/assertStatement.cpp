/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "assertStatement.h"

#include "plugins/ecmascript/es2panda/binder/ETSBinder.h"
#include "plugins/ecmascript/es2panda/compiler/base/condition.h"
#include "plugins/ecmascript/es2panda/compiler/core/pandagen.h"
#include "plugins/ecmascript/es2panda/compiler/core/ETSGen.h"
#include "plugins/ecmascript/es2panda/checker/ETSchecker.h"
#include "plugins/ecmascript/es2panda/checker/TSchecker.h"
#include "plugins/ecmascript/es2panda/ir/astDump.h"
#include "plugins/ecmascript/es2panda/ir/expression.h"

namespace panda::es2panda::ir {
void AssertStatement::Iterate(const NodeTraverser &cb) const
{
    cb(test_);

    if (second_ != nullptr) {
        cb(second_);
    }
}

void AssertStatement::Dump(ir::AstDumper *dumper) const
{
    dumper->Add({{"type", "AssertStatement"}, {"test", test_}, {"second", AstDumper::Nullable(second_)}});
}

void AssertStatement::Compile([[maybe_unused]] compiler::PandaGen *pg) const {}

void AssertStatement::ThrowError(compiler::ETSGen *const etsg) const
{
    const compiler::RegScope rs(etsg);

    if (second_ != nullptr) {
        second_->Compile(etsg);
    } else {
        etsg->LoadAccumulatorString(this, "Assertion failed.");
    }

    const auto message = etsg->AllocReg();
    etsg->StoreAccumulator(this, message);

    const auto assertion_error = etsg->AllocOutReg(etsg->Checker()->GlobalETSObjectType());
    etsg->NewObject(this, assertion_error, compiler::Signatures::BUILTIN_ASSERTION_PANIC);
    etsg->CallThisStatic1(this, assertion_error, compiler::Signatures::BUILTIN_ASSERTION_PANIC_CTOR, message);
    etsg->EmitThrow(this, assertion_error);
}

void AssertStatement::Compile([[maybe_unused]] compiler::ETSGen *etsg) const
{
    auto res = compiler::Condition::CheckConstantExpr(test_);

    if (res == compiler::Condition::Result::CONST_TRUE) {
        return;
    }

    if (res == compiler::Condition::Result::CONST_FALSE) {
        ThrowError(etsg);
        return;
    }

    compiler::Label *end_label = etsg->AllocLabel();

    test_->Compile(etsg);
    etsg->BranchIfTrue(this, end_label);
    ThrowError(etsg);
    etsg->SetLabel(this, end_label);
}

checker::Type *AssertStatement::Check([[maybe_unused]] checker::TSChecker *checker)
{
    return nullptr;
}

checker::Type *AssertStatement::Check([[maybe_unused]] checker::ETSChecker *checker)
{
    auto *test_type = test_->Check(checker);

    checker->CheckTruthinessOfType(test_type, test_->Start());

    if (second_ != nullptr) {
        auto *msg_type = second_->Check(checker);

        if (!msg_type->IsETSStringType()) {
            checker->ThrowTypeError("Assert message must be string", second_->Start());
        }
    }

    return nullptr;
}
}  // namespace panda::es2panda::ir
