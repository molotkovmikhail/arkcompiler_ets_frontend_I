/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ES2PANDA_UTIL_HELPERS_H
#define ES2PANDA_UTIL_HELPERS_H

#include "plugins/ecmascript/es2panda/binder/variableFlags.h"
#include "mem/pool_manager.h"
#include "plugins/ecmascript/es2panda/util/ustring.h"
#include "plugins/ecmascript/es2panda/ir/module/importSpecifier.h"

#include <cmath>

namespace panda::es2panda::checker {
class ETSObjectType;
class Type;
}  // namespace panda::es2panda::checker

namespace panda::es2panda::compiler {
class Literal;
}  // namespace panda::es2panda::compiler

namespace panda::es2panda::ir {
class Expression;
class ScriptFunction;
class ClassDefinition;
class ClassProperty;
class Identifier;
class MethodDefinition;
class AstNode;
class ClassStaticBlock;
class TSInterfaceDeclaration;
class TSEnumDeclaration;
enum class AstNodeType;
}  // namespace panda::es2panda::ir

namespace panda::es2panda::util {
enum class LogLevel : std::uint8_t {
    DEBUG,
    INFO,
    WARN,
    ERROR,
    FATAL,
};

class Helpers {
public:
    Helpers() = delete;

    static bool IsGlobalIdentifier(const util::StringView &str);
    static bool ContainSpreadElement(const ArenaVector<ir::Expression *> &args);
    static util::StringView LiteralToPropName(const ir::Expression *lit);

    template <typename T>
    static bool IsInteger(double number);
    static bool IsIndex(double number);
    static int64_t GetIndex(const util::StringView &str);

    static std::string ToString(double number);
    static util::StringView ToStringView(ArenaAllocator *allocator, double number);
    static util::StringView ToStringView(ArenaAllocator *allocator, int32_t number);
    static util::StringView ToStringView(ArenaAllocator *allocator, uint32_t number);
    static bool IsRelativePath(const std::string &path);

    static const ir::ScriptFunction *GetContainingConstructor(const ir::AstNode *node);
    static const ir::ScriptFunction *GetContainingConstructor(const ir::ClassProperty *node);
    static ir::AstNode *FindAncestorGivenByType(ir::AstNode *node, ir::AstNodeType type);

    static const checker::ETSObjectType *GetContainingObjectType(const ir::AstNode *node);
    static const ir::TSEnumDeclaration *GetContainingEnumDeclaration(const ir::AstNode *node);
    static const ir::ClassDefinition *GetContainingClassDefinition(const ir::AstNode *node);
    static const ir::TSInterfaceDeclaration *GetContainingInterfaceDeclaration(const ir::AstNode *node);
    static const ir::MethodDefinition *GetContainingClassMethodDefinition(const ir::AstNode *node);
    static const ir::ClassStaticBlock *GetContainingClassStaticBlock(const ir::AstNode *node);
    static const ir::ScriptFunction *GetContainingFunction(const ir::AstNode *node);
    static const ir::ClassDefinition *GetClassDefiniton(const ir::ScriptFunction *node);
    static bool IsSpecialPropertyKey(const ir::Expression *expr);
    static bool IsConstantPropertyKey(const ir::Expression *expr, bool is_computed);
    static compiler::Literal ToConstantLiteral(const ir::Expression *expr);
    static bool IsBindingPattern(const ir::AstNode *node);
    static bool IsPattern(const ir::AstNode *node);
    static std::vector<ir::Identifier *> CollectBindingNames(ir::AstNode *node);
    static util::StringView FunctionName(ArenaAllocator *allocator, const ir::ScriptFunction *func);
    static void CheckImportedName(ArenaVector<ir::AstNode *> *specifiers, const ir::ImportSpecifier *specifier,
                                  const std::string &file_name);
    static std::tuple<util::StringView, bool> ParamName(ArenaAllocator *allocator, const ir::AstNode *param,
                                                        uint32_t index);
    static const checker::Type *GetTypeFromTypeRef(const checker::Type *type_ref);

    template <typename Source, typename Target>
    static bool IsTargetFitInSourceRange(Target target)
    {
        if (!std::isfinite(target)) {
            return true;
        }

        // NOLINTNEXTLINE(misc-redundant-expression)
        return target >= std::numeric_limits<Source>::lowest() &&
               target <= static_cast<Target>(std::numeric_limits<Source>::max());
    }

    static const uint32_t INVALID_INDEX = 4294967295L;

    static std::string CreateEscapedString(const std::string &str);
    static std::string UTF16toUTF8(char16_t c);

    template <typename... Elements>
    static void LogDebug(Elements &&...elems);
    template <typename... Elements>
    static void LogInfo(Elements &&...elems);
    template <typename... Elements>
    static void LogWarn(Elements &&...elems);
    template <typename... Elements>
    static void LogError(Elements &&...elems);
    template <typename... Elements>
    static void LogFatal(Elements &&...elems);

private:
    template <LogLevel LOG_L, typename... Elements>
    static void Log(Elements &&...elems);
};

template <typename T>
bool Helpers::IsInteger(double number)
{
    if (std::fabs(number) <= static_cast<double>(std::numeric_limits<T>::max())) {
        T int_num = static_cast<T>(number);

        if (static_cast<double>(int_num) == number) {
            return true;
        }
    }

    return false;
}

template <LogLevel LOG_L, typename... Elements>
void Helpers::Log(Elements &&...elems)
{
    constexpr auto LOGGER = [](auto &&element) {
        using Element = decltype(element);

        if constexpr (LOG_L == LogLevel::DEBUG) {
            LOG(DEBUG, ES2PANDA) << std::forward<Element>(element);
        } else if constexpr (LOG_L == LogLevel::INFO) {
            LOG(INFO, ES2PANDA) << std::forward<Element>(element);
        } else if constexpr (LOG_L == LogLevel::WARN) {
            LOG(WARNING, ES2PANDA) << std::forward<Element>(element);
        } else if constexpr (LOG_L == LogLevel::ERROR) {
            LOG(ERROR, ES2PANDA) << std::forward<Element>(element);
        } else if constexpr (LOG_L == LogLevel::FATAL) {
            LOG(FATAL, ES2PANDA) << std::forward<Element>(element);
        } else {
            UNREACHABLE();
        }
    };
    (LOGGER(std::forward<Elements>(elems)), ...);
}

template <typename... Elements>
void Helpers::LogDebug(Elements &&...elems)
{
    Helpers::Log<LogLevel::DEBUG>(std::forward<Elements>(elems)...);
}

template <typename... Elements>
void Helpers::LogInfo(Elements &&...elems)
{
    Helpers::Log<LogLevel::INFO>(std::forward<Elements>(elems)...);
}

template <typename... Elements>
void Helpers::LogWarn(Elements &&...elems)
{
    Helpers::Log<LogLevel::WARN>(std::forward<Elements>(elems)...);
}

template <typename... Elements>
void Helpers::LogError(Elements &&...elems)
{
    Helpers::Log<LogLevel::ERROR>(std::forward<Elements>(elems)...);
}

template <typename... Elements>
void Helpers::LogFatal(Elements &&...elems)
{
    Helpers::Log<LogLevel::FATAL>(std::forward<Elements>(elems)...);
}
}  // namespace panda::es2panda::util

#endif
